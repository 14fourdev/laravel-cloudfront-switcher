<?php
/**
 * Cloudfront
 *
 * Configuration for the CloudFrontManger package
 *
 * @category Library
 * @package  CloudFrontManger
 * @author   Josh Hagel <josh@14four.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @link     https://14four.com
 * @since    1.0.0
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Enable The CloudFront Manager
    |--------------------------------------------------------------------------
    |
    | This option Sets the cloud front manager to active. This will protect
    | your system from being able to adjust the CloudFront origin without
    | specifically being allowed to.
    |
    | Supported: true, false
    |
    */
    'enabled' => env('CLOUDFRONT_MANAGER', false),

    /*
    |--------------------------------------------------------------------------
    | Set the ID for your CloudFront Distribution
    |--------------------------------------------------------------------------
    |
    | Sets the CloudFront distribution ID that you should be monitoring to
    | take switch the Origins
    |
    | Supported: string
    |
    */
    'id' => env('CLOUDFRONT_ID'),

    /*
    |--------------------------------------------------------------------------
    | CREDENTIALS FOR AWS
    |--------------------------------------------------------------------------
    |
    | Set the credentials to be used when authenticating with AWS to check
    | CloudFront origin and update the desired origin.
    |
    */
    'key' => env('AWS_ACCESS_KEY_ID'),

    'secret' => env('AWS_SECRET_ACCESS_KEY'),

    'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),


    /*
    |--------------------------------------------------------------------------
    | ORIGIN IDs
    |--------------------------------------------------------------------------
    |
    | Define the Origins Id that are to be used for the key. This should match
    | the definitions within the CloudFront Dashboard under "Origins and
    | Origin Groups"
    |
    | Example: 'blank' => env('CLOUDFRONT_PROD', 'production-s3-bucket'),
    |
    */
    'origins' => [
        // 'blank' => env('CLOUDFRONT_BLANK', 'origin-id'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Behaviors
    |--------------------------------------------------------------------------
    |
    | Defines the Dates that a specific Behavior should point to a specific
    | origin. A date is the when an origin key should be live. It will use the
    | last date that has passed as the current origin that should be used.
    | Use 'DefaultCacheBehavior' to define the default behavior, you can also
    | define other behaviors in the configuration.
    |
    | Requires: keys ['date', 'origin']
    |
    | Example:
    |   [
    |       'date' => env('CLOUDFRONT_PRE_DATE', '2019-08-01 00:00:00'),
    |       'origin' => 'blank',
    |   ],
    |
    */
    'behaviors' => [
        'DefaultCacheBehavior' => [
            // [
            //     'date' => env('CLOUDFRONT_PRE_DATE', '2019-08-01 00:00:00'),
            //     'origin' => 'blank',
            // ],
        ],
    ],
];

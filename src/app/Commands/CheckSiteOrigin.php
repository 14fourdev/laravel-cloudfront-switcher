<?php
/**
 * CheckSiteOrigin
 *
 * Command to check CloudFront origin
 *
 * @category Library
 * @package  CloudFrontManger
 * @author   Josh Hagel <josh@14four.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @link     https://14four.com
 * @since    1.0.0
 */
namespace CloudFrontManger\Commands;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Aws\CloudFront\CloudFrontClient;
use Illuminate\Support\Facades\Config;

/**
 * Check the site origin within CloudFront
 *
 * @category Console
 * @package  CloudFrontManger
 * @author   Josh Hagel <josh@14four.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @link     http://14four.com
 */
class CheckSiteOrigin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloudfront:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the current site origin';

    /**
     * AWS client for connecting to CloudFront
     *
     * @var Aws\CloudFront\CloudFrontClient
     */
    protected $client;

    /**
     * Track if an update had been triggered
     *
     * @var boolean
     */
    protected $updated = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!Config::get('cloudfront.enabled')) {
            $this->error('Please set CLOUDFRONT_MANAGER=true');
            return;
        }

        $this->_getClient();
        $id = Config::get('cloudfront.id');

        if ($id == "") {
            $this->error('Please set your Couldfront ID');
            return;
        }

        $this->info("Checking Origin {$id}");

        $distribution = $this->client->getDistribution(['Id' => $id]);

        $shouldBe = $this->_checkBehavior(
            'DefaultCacheBehavior',
            $distribution["Distribution"]["DistributionConfig"]['DefaultCacheBehavior']
        );

        if ($shouldBe) {
            $distribution["Distribution"]["DistributionConfig"]['DefaultCacheBehavior']['TargetOriginId'] = $shouldBe;
            $this->updated = true;
        }

        foreach ($distribution["Distribution"]["DistributionConfig"]['CacheBehaviors']['Items'] as $key => $behavior) {
            $shouldBe = $this->_checkBehavior($behavior['PathPattern'], $distribution["Distribution"]["DistributionConfig"]['DefaultCacheBehavior']);
            if ($shouldBe) {
                $distribution["Distribution"]["DistributionConfig"]['CacheBehaviors']['Items'][$key]['TargetOriginId'] = $shouldBe;
                $this->updated = true;
            }
        }

        if ($this->updated) {
            try {
                $result = $this->client->updateDistribution(
                    [
                        'DistributionConfig' => $distribution["Distribution"]["DistributionConfig"],
                        'Id' => $id,
                        'IfMatch' => $distribution['ETag'],
                    ]
                );

                $result = $this->client->createInvalidation(
                    [
                        'DistributionId' => $id,
                        'InvalidationBatch' => [
                            'CallerReference' => 'switch',
                            'Paths' => [
                                'Items' => ['/*'],
                                'Quantity' => 1,
                            ],
                        ]
                    ]
                );
            } catch (AwsException $e) {
                // output error message if fails
                echo $e->getMessage();
            }
        } else {
            $this->info("Nothing to update");
        }
    }

    /**
     * Check if a handle should be updated
     *
     * @param string $handle   handle of the behavior that should be checked
     * @param string $behavior behavior that is being checked
     *
     * @return string
     */
    private function _checkBehavior($handle, $behavior)
    {
        $shouldBeOrigin = $this->_behaviorsOrigin($handle);

        if (!$shouldBeOrigin) return;

        if ($behavior['TargetOriginId'] != $shouldBeOrigin) {
            $this->info("{$handle} should be {$shouldBeOrigin}");
            return $shouldBeOrigin;
        }

        return;
    }


    /**
     * Get the CloudFront Client
     *
     * @return void
     */
    private function _getClient()
    {
        $this->client = new CloudFrontClient(
            [
                'version' => 'latest',
                'region' => Config::get('cloudfront.region'),
                'credentials' => [
                    'key'    => Config::get('cloudfront.key'),
                    'secret' => Config::get('cloudfront.secret'),
                ]
            ]
        );
    }

    /**
     * Get the origin that should be set to live
     *
     * @param string $behaviorName Name of the behvior that is getting checked
     *
     * @return array
     */
    private function _behaviorsOrigin($behaviorName)
    {
        $now = Carbon::now();

        $dates = collect(Config::get("cloudfront.behaviors.{$behaviorName}"));

        if (empty($dates)) {
            return null;
        }

        $sorted = $dates->map(
            function ($config) {
                $config['date'] = Carbon::parse($config['date']);
                return $config;
            }
        )->sortBy('date');

        $matched;

        foreach ($sorted as $key => $value) {
            if ($value['date'] <= $now) {
                $matched = $value;
            }
        }

        if (empty($matched)) return;

        return Config::get("cloudfront.origins.{$matched['origin']}");
    }
}

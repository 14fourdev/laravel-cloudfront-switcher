<?php
/**
 * CloudFrontServiceProvider
 *
 * Service Provider to register Commands and publishing of config
 *
 * @category Library
 * @package  CloudFrontManger
 * @author   Josh Hagel <josh@14four.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @link     https://14four.com
 * @since    1.0.0
 */
namespace CloudFrontManger;

use Illuminate\Support\ServiceProvider;

/**
 * Laravel Service provider for the CloudFrontManager Package.
 *
 * @category ServiceProvider
 * @package  CloudFrontManger
 * @author   Josh Hagel <josh@14four.com>
 * @license  MIT <https://opensource.org/licenses/MIT>
 * @link     http://14four.com
 */
class CloudFrontServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // REGISTER COMMANDS
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    \CloudFrontManger\Commands\CheckSiteOrigin::class,
                ]
            );
        }

        // PUBLISH
        $this->registerPublishes();
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }


    /**
     * Register Publish Tags
     *
     * @return void
     */
    public function registerPublishes()
    {
        $baseDir = __DIR__ . '/..';

        $date = date('Y_m_d_His');


        // PUBLISH CONFIG FILES
        $this->publishes(
            [
                "$baseDir/config/cloudfront.php" => config_path('cloudfront.php'),
            ],
            'config'
        );
    }
}

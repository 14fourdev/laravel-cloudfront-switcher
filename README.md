# Cloud Front Switcher

The CloudFront manager will switch which origin is being used for a CloudFront origin.

[toc]

## Purpose

The purpose of this package is to automatically switch origins when particular times are passed.

This allows us to host static files in S3 buckets and switch which bucket is used at particular times.

## Installation

### Add to Composer

`composer require 14four/laravel-cloudfront`

### Publish Config file

`php artisan vendor:publish --provider="CloudFrontManger\CloudFrontServiceProvider" --tag="config"`

### Update Configuration

#### ID

Set the CloudFront id that you'll be targeting... suggest using `env()` so that you can overwrite this for stage and production.

**EXAMPLE**

```php
'id' => env('CLOUDFRONT_ID', 'productionid'),
```

#### CREDENTIALS

These will be the credentials that you will be using to access CloudFront. Make sure that you are setting providing credentials that can read and write to CloudFront.

*Set these in your .env so you don't compromise your security*

**EXAMPLE**

```
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
```

#### Origins

Set Origins in the configuration. These need to match the `Origin ID` from the CloudFront Distribution Your Targeting.

**EXAMPLE**

```php
  'origins' => [
      'blank' => env('CLOUDFRONT_BLANK', 'origin-id'),
      'comingSoon' => env('CLOUDFRONT_COMINGSOON', 's3-coming-soon-bucket'),
      'live' => env('CLOUDFRONT_LIVE', 's3-live-bucket'),
      'over' => env('CLOUDFRONT_OVER', 's3-over-bucket'),
  ],
```

#### Behaviors

Map your behaviors to dates and the origin that should be live. You can switch multiple behaviors (default is provided).

Each behaviors will be checked based on the last date found that has passed.

**EXAMPLE**

```php
    'behaviors' => [
        'DefaultCacheBehavior' => [
            [
                'date' => env('CLOUDFRONT_PRE_DATE', '2019-08-01 00:00:00'),
                'origin' => 'blank',
            ],
            [
                'date' => env('CLOUDFRONT_COMING_DATE', '2019-08-02 00:00:00'),
                'origin' => 'comingSoon',
            ],
            [
                'date' => env('CLOUDFRONT_LIVE_DATE', '2019-09-02 12:00:00'),
                'origin' => 'live',
            ],
            [
                'date' => env('CLOUDFRONT_OVER_DATE', '2019-11-20 23:59:59'),
                'origin' => 'over',
            ],
        ],
    ],
```

### Add to Scheduler

In the `app/Console/Kernel.php` file add the `cloudfront:check` command

```php
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('cloudfront:check')
            ->everyMinute();
    }
```
